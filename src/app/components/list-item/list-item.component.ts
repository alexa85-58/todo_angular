import { emitDistinctChangesOnlyDefaultValue } from '@angular/compiler/src/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToDoItem } from 'src/app/interfaces/todo';
// import { EventEmitter } from 'stream';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {
    @Input() index: number;
    @Input() item: ToDoItem;
    @Output('remove') onRemove: EventEmitter<number> = new EventEmitter<number>();
    @Output('edit') onEdit: EventEmitter<number> = new EventEmitter<number>();
    
    constructor() {
      this.item = {};
      this.index = 0;

    }  

    edit(): void {
      this.onEdit.emit(
        this.index
      );

    }

    remove(): void {
      this.onRemove.emit(
        this.index
      );
    }
}
