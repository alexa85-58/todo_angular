import { isNgTemplate } from '@angular/compiler';
import { LIFECYCLE_HOOKS_VALUES } from '@angular/compiler/src/lifecycle_reflector';
import { Component, OnInit } from '@angular/core';
import { ToDoItem } from 'src/app/interfaces/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {
  inputText: string = '';

  list: ToDoItem[] = [
    { title: 'Покормить кота', done: false },
    { title: 'Купить хлеб', done: true },
   ]; 

   showClass = false;

  addItem(): void{
    const title: string = this.inputText;
    const toDo = { title, done: false };
    this.list.push(toDo);
    this.inputText = '';
  }

  remove(idx: number) {
    this.list = this.list.filter(
     (item: ToDoItem, i: number) => idx !== i
    );
  }

edit(idedit: number) {
  this.list = this.list.filter(
    (item: ToDoItem, i: number) => idedit === i
   );
   this.inputText = '';

}

}
